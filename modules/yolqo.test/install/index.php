<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\IO\Directory;
use \Bitrix\Main\ModuleManager;
use \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

Class yolqo_test extends \CModule
{
    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public function __construct()
    {
        $arModuleVersion = [];
        include(dirname(__FILE__) . "/version.php");

        $this->MODULE_NAME = "Yolqo.Test";
        $this->MODULE_DESCRIPTION = "Модуль с компонентами тестового задания";
        $this->MODULE_ID = 'yolqo.test';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = 'Yolqo';
        $this->PARTNER_URI = 'soon';
    }

    public function installComponents()
    {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/yolqo.test/install/components',
            $_SERVER['DOCUMENT_ROOT'] . '/local/components', true, true);

        return true;
    }

    public function uninstallComponents()
    {
        Directory::deleteDirectory('/local/components/yolqo');

        return true;
    }

    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        if (Loader::includeModule($this->MODULE_ID)) {
            $this->installComponents();
        }
    }

    public function DoUninstall()
    {
        $this->uninstallComponents();
        ModuleManager::unregisterModule($this->MODULE_ID);

        return true;
    }
}
