<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
    'NAME'        => Loc::getMessage('NAME'),
    'DESCRIPTION' => Loc::getMessage('DESC'),
    'SORT'        => 10,
    'CACHE_PATH'  => 'Y',
    'PATH'        => [
        'ID'    => 'content',
        'CHILD' => [
            'ID'   => 'test',
            'NAME' => Loc::getMessage('TEST_NAME'),
            'SORT' => 10,
        ]
    ],
];
