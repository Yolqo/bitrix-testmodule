<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;

/**
 * Class GetRequestList
 */
class GetRequestList extends \CBitrixComponent
{
    public function executeComponent()
    {
        if (!Loader::includeModule('iblock')) {
            return;
        }

        $param = htmlspecialchars(stripslashes($this->arParams['GET_PARAM']));

        if (!$param) {
            return;
        }

        if ($this->StartResultCache($this->arParams['CACHE_TIME'], $param)) {
            $this->arResult['ITEMS'] = $this->getItems($param);
            $this->arResult['GET_PARAMETER'] = $param;

            $this->setSectionNames();

            if (count($this->arResult['ITEMS']) < 1) {
                $this->AbortResultCache();
            }

            $this->includeComponentTemplate();
        }
    }

    /**
     * Получение элементов с фильтрацией по значению из Get-параметра
     *
     * @param $name
     * @return mixed
     */
    protected function getItems($name)
    {
        try {
            $result = ElementTable::query()
                ->setSelect(['NAME', 'IBLOCK_SECTION_ID'])
                ->setFilter([
                    'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                    'NAME' => '%' . $name . '%'
                ])
                ->setLimit($this->arParams['LIMIT'])
                ->exec()
                ->fetchAll();
        } catch (\Bitrix\Main\ObjectPropertyException $e) {
            echo $e->getMessage(); //@todo обработка
        } catch (\Bitrix\Main\ArgumentException $e) {
            echo $e->getMessage(); //@todo обработка
        } catch (\Bitrix\Main\SystemException $e) {
            echo $e->getMessage(); //@todo обработка
        }

        return $result;
    }

    /**
     * Установка имен секций
     */
    protected function setSectionNames()
    {
        $sectionsId = [];

        foreach ($this->arResult['ITEMS'] as $item) {
            $sectionsId[] = $item['IBLOCK_SECTION_ID'];
        }

        $sections = SectionTable::query()
            ->setSelect(['ID', 'NAME'])
            ->setFilter([
                'ID' => $sectionsId,
            ])
            ->exec();

        while ($section = $sections->fetch()) {
            $this->arResult['SECTIONS'][$section['ID']] = $section['NAME'];
        }
    }
}
