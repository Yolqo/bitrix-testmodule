<?php
/** @var array $arCurrentValues */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

if (!Loader::includeModule('iblock'))
    return;

$arIBlocks = [];
$dbIblock = CIBlock::GetList(['SORT' => 'ASC'], ['SITE_ID' => $_REQUEST['site'], 'TYPE' => ($arCurrentValues['IBLOCK_TYPE'] != '-' ? $arCurrentValues['IBLOCK_TYPE'] : '')]);

while ($arRes = $dbIblock->Fetch()) {
    $arIBlocks[$arRes['ID']] = '[' . $arRes['ID'] . '] ' . $arRes['NAME'];
}

$arComponentParameters = [
    'PARAMETERS' => [
        'IBLOCK_ID'   => [
            'PARENT'  => 'BASE',
            'NAME'    => Loc::getMessage('IBLOCK_NAME'),
            'TYPE'    => 'LIST',
            'VALUES'  => $arIBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ],
        'GET_PARAM' => [
            'PARENT' => 'SETTINGS',
            'NAME' => Loc::getMessage('GET_PARAM_NAME'),
            'TYPE' => 'STRING',
            'DEFAULT' => $_REQUEST['name'],
        ],
        'LIMIT' => [
            'PARENT' => 'SETTINGS',
            'NAME' => Loc::getMessage('LIMIT'),
            'TYPE' => 'STRING',
            'DEFAULT' => 10,
        ],
        'CACHE_TIME' => ['DEFAULT' => 36000000],
    ]
];
