<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>

<?php foreach ($arResult['ITEMS'] as $item) : ?>

    <?php if ($arResult['SECTIONS'][$item['IBLOCK_SECTION_ID']]) : ?>
        <h1><?= $arResult['SECTIONS'][$item['IBLOCK_SECTION_ID']] ?></h1>
    <?php endif; ?>

    <h2><?= $item['NAME'] ?></h2>
    <hr>

<?php endforeach; ?>