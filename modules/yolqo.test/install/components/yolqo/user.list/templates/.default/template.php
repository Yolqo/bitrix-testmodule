<?php
CJSCore::Init(['jquery']);
?>

<div id="users">
    <?php foreach ($arResult['USERS'] as $key => $user) : ?>
        <p><?= $user['NAME'] . ' ' . $user['LAST_NAME'] ?></p>
    <?php endforeach; ?>
</div>

<?php
while ($arResult['PAGE'] <= $arResult['PAGE_COUNT']) : ?>
    <button class="button" data-value="<?= $arResult['PAGE'] ?>"><?= $arResult['PAGE'] ?></button>
    <?php
    $arResult['PAGE']++;
endwhile; ?>

<script>
    window.onload = function () {
        $('.button').on('click', function () {
            var query = {
                c: 'yolqo:user.list',
                action: 'page',
                mode: 'class'
            };

            var data = {
                page: this.dataset.value,
                limit: <?=$arResult['COUNT']?>,
                SITE_ID: 's1',
            };

            var request = $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
                method: 'POST',
                data: data
            });

            request.done(function (response) {
                $("#users").empty();
                for (userKey in response.data.users) {
                    $('#users').append('<p>' + response.data.users[userKey].NAME + ' ' + response.data.users[userKey].LAST_NAME + '</p>');
                }
            });
        });
    };
</script>