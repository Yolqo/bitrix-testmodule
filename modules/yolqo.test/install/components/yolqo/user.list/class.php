<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use \Bitrix\Main\UserTable;
use \Bitrix\Main\Engine\Contract\Controllerable;
use \Bitrix\Main\Engine\ActionFilter;

/**
 * Class UserList
 */
class UserList extends \CBitrixComponent implements Controllerable
{
    public function configureActions()
    {
        return [
            'page' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_GET]
                    ),
                ],
                'postfilters' => []
            ]
        ];
    }

    public function executeComponent()
    {
        $this->arResult['COUNT'] = $this->arParams['COUNT'];

        $nav = $this->createNav($this->arResult['COUNT']);
        $users = $this->getUsers($nav);

        $nav->setRecordCount($users->getCount());

        while ($user = $users->fetch()) {
            $this->arResult['USERS'][] = $user;
        }

        $this->arResult['PAGE_COUNT'] = $nav->getPageCount();
        $this->arResult['PAGE'] = 1;

        $this->includeComponentTemplate();
    }

    public function pageAction($limit, $page)
    {
        $nav = $this->createNav($limit, $page);

        $users = $this->getUsers($nav);

        return ['users' => $users->fetchAll()];
    }

    protected function createNav($limit, $page = 1)
    {
        $nav = new \Bitrix\Main\UI\PageNavigation('nav-more-users');

        $nav->allowAllRecords(true)
            ->setPageSize($limit)
            ->setCurrentPage($page);

        return $nav;
    }

    protected function getUsers($nav)
    {
        $users = UserTable::query()
            ->setSelect(['NAME', 'LAST_NAME'])
            ->setLimit($nav->getLimit())
            ->setOffset($nav->getOffset())
            ->countTotal(true)
            ->exec();

        return $users;
    }
}
